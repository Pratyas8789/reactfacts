import React from 'react'
export default function NavBar() {
    return (
        <div className='navBar'>
            <div className='navIcon'>
                <img src="./logo192.png" alt="fghg" />
                <h3>ReactFacts</h3>
            </div>
            <h4>React Course - Project 1</h4>
        </div>
    )
}
