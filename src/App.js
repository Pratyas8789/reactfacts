import logo from './logo.svg';
import './App.css';
import NavBar from './component/NavBar';
import MainComponent from './component/MainComponent';

function App() {
  return (
    <div className="App">
      <NavBar/>
      <MainComponent/>
    </div>
  );
}

export default App;
